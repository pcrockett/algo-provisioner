Algo Provisioner
================

A small Vagrant virtual machine that includes everything you need to provision a new [Algo][1] server. Once you have Vagrant and VirtualBox installed on your local computer, simply run

    vagrant up

Once it's finished provisioning, run

    vagrant ssh
    cd ./algo/

    # Edit your Algo server settings
    nano config.cfg

    # Provision your Algo server
    ./algo

Keep this virtual machine around. You'll need it to manage your VPN server. For information on how to actually manage your Algo server, check out the [Algo README][2].

[1]: https://github.com/trailofbits/algo
[2]: https://github.com/trailofbits/algo/blob/master/README.md